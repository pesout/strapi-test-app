module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '2c4d9df9d1aa4b1237fe4acfb16236cc'),
  },
});
